package eze.chess;
//该类展示了五子棋存档读档的功能
import java.io.*;

public class Demo_chess {
    public static void main(String[] args) throws IOException {
        //创建一个11*11的二维数组
        //0为无子1位黑子2为蓝子
        int[][] chessArr1 = new int[11][11];
        chessArr1[1][2] = 1;
        chessArr1[2][3] = 2;
        chessArr1[5][3] = 1;
        chessArr1[6][9] = 2;
        //输出原始二维数组
        for (int[] row : chessArr1) {
            for (int data : row) {
                System.out.printf("%d\t", data);
            }
            System.out.println();
        }
        //遍历二维数组  得到非0的值的个数
        int sum = 0;
        for (int i = 0; i < chessArr1.length; i++) {
            for (int j = 0; j < chessArr1[i].length; j++) {
                if (chessArr1[i][j] != 0) {
                    sum++;
                }
            }
        }
        //创建稀疏数组
        int[][] sparseArr = new int[sum + 1][3];
        //给稀疏数组赋值
        sparseArr[0][0] = 11;
        sparseArr[0][1] = 11;
        sparseArr[0][2] = sum;
        //遍历二维数组将值赋值给稀疏数组
        int count = 0;//用于记录第几个非0数据
        for (int i = 0; i < chessArr1.length; i++) {
            for (int j = 0; j < chessArr1[i].length; j++) {
                if (chessArr1[i][j] != 0) {
                    count++;
                    sparseArr[count][0] = i;
                    sparseArr[count][1] = j;
                    sparseArr[count][2] = chessArr1[i][j];
                }
            }
        }
        //输出稀疏数组
        System.out.println("稀疏数组是——————————————");
        for (int i = 0; i < sparseArr.length; i++) {
            for (int j = 0; j < sparseArr[i].length; j++) {
                System.out.printf("%d\t", sparseArr[i][j]);
            }
            System.out.println();
        }
        //将稀疏数组恢复成原来的二维数组
        //先读取第一行
        int[][] chessArr2 = new int[sparseArr[0][0]][sparseArr[0][1]];
        for (int i = 1; i < sparseArr.length; i++) {
            chessArr2[sparseArr[i][0]][sparseArr[i][1]] = sparseArr[i][2];
        }
        System.out.println("还原的二维数组为————————");
        for (int[] row : chessArr2) {
            for (int data : row) {
                System.out.printf("%d\t", data);
            }
            System.out.println();
        }
        //将稀疏数组保存进入txt文件中
        FileWriter fileOut = new FileWriter(new File("D:\\chess\\1.txt"));
        for (int i=0;i< sparseArr.length;i++){
            for(int j=0;j<sparseArr[i].length;j++){
                String content=String.valueOf(sparseArr[i][j]);
                fileOut.write(content+"\t");
            }
            fileOut.write("\r\n");
        }
        fileOut.close();
        //读取txt文件将其输出为稀疏数组
        int[][] sparseArr2 = new int[sum+1][3];
        InputStreamReader in = new InputStreamReader(new FileInputStream("D:\\chess\\1.txt"));
        BufferedReader fileIn = new BufferedReader(in);
        String line = null;
        int i= 0;
        while ((line = fileIn.readLine()) != null ){
            if(line != null){
                String[] s = line.split("\\t");
                for(int j=0;j<s.length;j++){
                    sparseArr2[i][j] = Integer.valueOf(s[j]);
                }
                i++;
            }
        }
        //输出读取的稀疏数组
        System.out.println("读取的数据为");
        for (int[] row : sparseArr2) {
            for (int data : row) {
                System.out.printf("%d\t", data);
            }
            System.out.println();
        }
        //将稀疏数组转换为原二维数组
        int[][] chessArr3 = new int[sparseArr2[0][0]][sparseArr2[0][1]];
        for (int k=1;k<sparseArr2.length;k++){
            chessArr3[sparseArr2[k][0]][sparseArr2[k][1]] = sparseArr2[k][2];
        }
        //输出还原的二维数组
        System.out.println("读取还原的二维数组为：");
        for (int[] row : chessArr3) {
            for (int data : row) {
                System.out.printf("%d\t", data);
            }
            System.out.println();
        }

    }
}