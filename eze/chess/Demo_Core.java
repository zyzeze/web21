package eze.chess;

import java.util.Stack;

public class Demo_Core {
    //棋盘大小
    private int[][] core;
    private int x;
    private int y;
    //记录下棋的类
    class Chess{
        int x;
        int y;
        public Chess(int x,int y){
            this.x=x;
            this.y=y;
        }
    }
    //栈
    Stack<Chess> stack;
    //构造方法
    public  Demo_Core(int x,int y){
        stack = new Stack<>();
        core = new int[x][y];
        this.x = x;
        this.y = y;
    }
    //检查空位
    private boolean canInput(int x,int y){
        if (core[x][y] == 0) return true;
        else return false;
    }


    /*@param y 横坐标
    /*@param x 纵坐标
    /*@param var 棋子种类  1 白棋  2  黑棋
    @return 1 白棋赢  2  黑棋赢
     */

    //  判断输赢的方法
    private int checkVictory(int x,int y,int var){
        // 横向判断
        int trans = 0;
        for(int i=y-4;i<y+5;i++){
            if(i<0 || i>=this.y){
                continue;
            }
            if(core[x][i] == var){
                trans++;
            }else {
                trans = 0;
            }
            if (trans == 5) return var;
        }
        //纵向判断
        int longitudinal = 0;
        for(int i=x-4;i<x+5;i++){
            if(i<0||i>=this.x){
                continue;
            }
            if(core[i][y] == var){
                longitudinal++;
            }else {
                longitudinal = 0;
            }
            if (longitudinal == 5) return var;
        }
        //左上到右下
        int left2 = 0;
        for(int i=x+4,j=y+4;i>x-5 && j>y-5;i--,j--){
            if (i<0 || i>=this.x || j<0 || j>=this.y){
                continue;
            }
            if (core[i][j] == var){
                left2++;
            }else {
                left2=0;
            }
            if (left2==5) return var;
        }
        //左下到右上
        int left1 = 0;
        for(int i=x+4,j=y-4;i>x-5 && j<y+5;i--,j++){
            if (i<0 || i>=this.x || j<0 || j>=this.y){
                continue;
            }
            if (core[i][j] == var){
                left1++;
            }else {
                left1=0;
            }
            if (left1==5) return var;
        }
        return 0;
    }
    public int ChessIt(int x,int y,int var) {
        if(canInput(x,y)) {
            core[x][y] =var;
            Chess chess = new Chess(x,y);
            stack.push(chess);
            return checkVictory(x, y, var);
        }
        else return -1;
    }
    //悔棋
    public boolean RetChess() {
        if(stack.isEmpty()) return false;
        Chess chess = stack.pop();
        core[chess.x][chess.y]= 0;
        return true;
    }
    //获得棋盘状态
    public int[][] getCore(){
        return this.core;
    }
    //重新开始
    public void Restart() {
        for(int i=0;i<this.x;i++) {
            for(int j=0;j<this.y;j++) {
                this.core[i][j]=0;
            }
        }
        this.stack.clear();
    }



}
